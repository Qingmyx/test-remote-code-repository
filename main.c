#include <stdio.h>
#include<stdbool.h>

/* 1990年1月1日 是星期一 */

bool is_leap_year(int year) 
{
    return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
}

int get_days_in_month(int month, bool leap) 
{
    int days_in_month[] = { 31, 28 + !!leap, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    return days_in_month[month - 1];
}
//12345567
int get_days_from_years(int year) 
{
    int days = 0;
    for (int i = 1990; i < year; i++) {
        days += is_leap_year(i) ? 366 : 365;
    }
    return days;
}

int get_days_from_months(int month, int leap)
 {
    int days = 0;
    for (int i = 1; i < month; i++) {
        days += get_days_in_month(i, leap);
    }
    return days;
}

void main() 
{
    int year, month, day, week, leap;
    int sum;

    printf("请输入日期（格式：年/月/日）：");
    scanf("%d/%d/%d", &year, &month, &day);

    leap = is_leap_year(year);
    sum = get_days_from_years(year) + get_days_from_months(month, leap) + day;
    
    week = sum % 7;

    if (week != 0) {
        printf("%d年%d月%d日是星期%d\n", year, month, day, week);
    } else {
        printf("%d年%d月%d日是星期日\n", year, month, day);
    }
}